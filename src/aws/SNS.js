const AWS = require('aws-sdk');
require('dotenv').config();

AWS.config.update({
    region: process.env.AWS_DEFAULT_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY 
});

const sns = new AWS.SNS();


const topicArn = 'arn:aws:sns:us-east-1:906647043531:Offers'; 


const subscribeOffers = async (email)=> {
    sns.subscribe({
        Protocol: 'email',
        TopicArn: topicArn,
        Endpoint: email
    }, (err, data) => {
        if (err) console.error(err, err.stack);
        else console.log(data.SubscriptionArn);
    });
}

const sendOffers = async (subject , message)=> {

    sns.publish({
        TopicArn: topicArn,
        Message: message,
        Subject: subject
    }, (err, data) => {
        if (err) console.error(err, err.stack);
        else console.log(data.MessageId);
    });
    
};

module.exports = {
    sendOffers,
    subscribeOffers
};

