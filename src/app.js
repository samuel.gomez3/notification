const express = require('express');
const { subscribeOffers , sendOffers} = require('./aws/SNS');

const app = express();

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Health')
});



app.post('/notification/subscribe', async (req, res) => {
    const email = req.body.email;

    try {
        const subscribe = await subscribeOffers(email);
        res.json(subscribe)
    } catch (err) {
        console.error(err);
        res.status(500).json({err: "something went wrong"})
    }

});


app.post('/notification/sendOffers', async (req, res) => {
    const subject = req.body.subject;
    const message = req.body.message;
    try {
        const offers = await sendOffers(subject,message );
        res.json(offers)
    } catch (err) {
        console.error(err);
        res.status(500).json({err: "something went wrong"})
    }

});



const port = process.env.PORT || 3001;

app.listen(port, () => {
    console.log('listening on port ')
})