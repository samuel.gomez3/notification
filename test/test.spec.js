const AWSMock = require('aws-sdk-mock');
const AWS = require('aws-sdk');
const { expect } = require('chai');
const { sendOffers, subscribeOffers } = require('../src/aws/SNS');

describe('SNS functions', () => {

    afterEach(() => {
        AWSMock.restore('SNS');
    });

    it('should subscribe an email to the offers topic', async () => {
        AWSMock.mock('SNS', 'subscribe', (params, callback) => {
            expect(params.Protocol).to.equal('email');
            expect(params.Endpoint).to.be.a('string');
            callback(null, { SubscriptionArn: 'testArn' });
        });

        const email = 'test@example.com';
        await subscribeOffers(email);
    });

    it('should send an offer message', async () => {
        AWSMock.mock('SNS', 'publish', (params, callback) => {
            expect(params.TopicArn).to.be.a('string');
            expect(params.Message).to.be.a('string');
            expect(params.Subject).to.be.a('string');
            callback(null, { MessageId: 'testMessageId' });
        });

        const subject = 'Test Subject';
        const message = 'Test Message';
        await sendOffers(subject, message);
    });

});

